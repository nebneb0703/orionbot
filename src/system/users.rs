use serenity::client::Context;
use serenity::http::Http;
use serenity::model::prelude::*;
use serenity::cache::Cache;

use diesel::prelude::*;
use diesel::SaveChangesDsl;

use futures::StreamExt;

use std::sync::Arc;
use crate::Db;
use crate::db::schema::users;
use nebbot_utils::prelude::*;

pub async fn join(ctx: Context, guild_id: GuildId, member: Member) {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    if let Some(u) = database.get_user(member.user.id.0, guild_id.0).await
        .expect("Failed to retreive user for on join caching.") 
    {
        let mut update = u.update();

        update.has_left = Some(false);

        let _: crate::db::DbUser = update.save_changes(&*database.conn.lock().await).expect("Failed to update user's existance.");
    }
}

pub async fn leave(ctx: Context, guild_id: GuildId, user: User) {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    if let Some(u) = database.get_user(user.id.0, guild_id.0).await
        .expect("Failed to retreive user for on leave caching.") 
    {
        let mut update = u.update();

        update.has_left = Some(true);

        let _: crate::db::DbUser = update.save_changes(&*database.conn.lock().await).expect("Failed to update user's existance.");
    }
}

pub async fn cache_users(http: Arc<Http>, cache: Arc<Cache>, database: Arc<Db>) {
    info!("Caching users.");
    
    'outer: 
    for guild_id in cache.guilds().await {
        let total_users = diesel::update(users::table)
            .set(users::has_left.eq(true))
            .filter(users::guild_id.eq(TextU64(guild_id.0)))
            .execute(&*database.conn.lock().await)
            .expect("Failed to update users to left");

        // If the server does not have any user entries, don't bother with the rest.
        if total_users == 0 {
            continue;
        }

        let mut members_iter = guild_id.members_iter(&http).boxed();

        let mut ids = Vec::new();

        while let Some(member_result) = members_iter.next().await {
            match member_result {
                Ok(member) => {
                    ids.push(TextU64(member.user.id.0));
                },
                Err(e) => {
                    error!("Cache users members iter error: {:?}", e);
                    continue 'outer;
                }
            }
        }

        let users = diesel::update(users::table)
            .set(users::has_left.eq(false))
            .filter(users::guild_id.eq(TextU64(guild_id.0)))
            .filter(users::discord_id.eq_any(ids))
            .execute(&*database.conn.lock().await)
            .expect("Failed to update users to left");

        info!("Noted {}/{} members in server {} ({})", 
            users, total_users,
            guild_id.to_partial_guild(&http).await.expect("Failed to get guild data.").name,
            guild_id.0);
    }

    info!("Finished caching users.");
}