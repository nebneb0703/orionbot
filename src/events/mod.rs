use serenity:: {
    async_trait,

    client::bridge::gateway::event::ShardStageUpdateEvent,
    gateway::ConnectionStage,

    model::{
        gateway::Ready,
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use chrono::prelude::*;
use nebbot_utils::time::human_readable;

mod drop;
pub use drop::*;

use crate::cache::*;
use crate::system;
pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let data = ctx.data.read().await;
        let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
        let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

        let time = Utc::now();

        system::users::cache_users(ctx.http.clone(), ctx.cache.clone(), database.clone()).await;
        system::roles::cache_roles(ctx.http.clone(), ctx.cache.clone(), database.clone(), bot_cache.clone()).await;
        system::invites::cache_invites(ctx.http.clone(), ctx.cache.clone(), database.clone()).await;

        let duration = Utc::now() - time;

        info!("Initialisation complete after {}.{}s!", human_readable(duration).trim_end_matches("s"), duration.num_milliseconds());
    }

    async fn resume(&self, _: Context, resume: ResumedEvent) {
        info!("Resumed! Trace: {:?}",  resume.trace);
    }

    async fn shard_stage_update(&self, _ctx: Context, update: ShardStageUpdateEvent) {
        info!("Shard {} update event: Old: {:?}; New: {:?}", update.shard_id, update.old, update.new);

        if let ConnectionStage::Connecting = update.new {
            warn!("Shard {} has disconnected! Reconnecting...", update.shard_id);
        }

        if let ConnectionStage::Resuming = update.new {
            warn!("Resuming Shard {}...", update.shard_id);
        }
    }

    async fn invite_create(&self, ctx: Context, event: InviteCreateEvent) {
        system::invites::invite_create(ctx, event).await;
    }

    async fn invite_delete(&self, ctx: Context, event: InviteDeleteEvent) {
        system::invites::invite_delete(ctx, event).await;
    }

    async fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, member: Member) {
        system::invites::join(ctx.clone(), guild_id, member.clone()).await;
        system::users::join(ctx, guild_id, member).await;
    }

    async fn guild_member_removal(&self, ctx: Context, guild_id: GuildId, user: User, _member_data_if_available: Option<Member>) {
        system::users::leave(ctx, guild_id, user).await;
    }
}

