use serenity::client::bridge::gateway::ShardManager;
use serenity::prelude::*;

use std::sync::Arc;
use std::collections::HashMap;

use chrono::prelude::*;

use parking_lot::Mutex as BlockingMutex;

use crate::db::Db;

pub struct DbContainer;

impl TypeMapKey for DbContainer {
    type Value = Arc<Db>;
}

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

pub struct BotCacheContainer;

impl TypeMapKey for BotCacheContainer {
    type Value = Arc<Mutex<BotCache>>;
}

pub struct NuggetDrop {
    pub guild_id: u64,
    pub time_start: DateTime<Utc>,
    pub time_last_message: DateTime<Utc>,
    pub messages: u32,
}

pub struct BotCache {
    pub deathmatch: Vec<u64>, // User Id
    pub spinning: HashMap<u64, BlockingMutex<Vec<u64>>>, // Server Id, User Id
    pub nugget_drop: HashMap<u64, NuggetDrop>, // Channel Id
    pub loot_dropping: HashMap<u64, bool>, // Server Id
    pub drop_cooldown: HashMap<u64, DateTime<Utc>>, // Server Id
    pub shutting_down: bool,
    pub richest: HashMap<u64, (u64, i64)>, // Server Id, (User Id, Nuggets)
    pub longest_spin: HashMap<u64, (u64, i64)>, // Server Id, (User Id, Duration)
    pub slots_cooldown: HashMap<u64, DateTime<Utc>>, // Server Id
}

impl BotCache {
    pub fn new() -> Self {
        BotCache {
            deathmatch: Vec::new(),
            spinning: HashMap::new(),
            nugget_drop: HashMap::new(),
            loot_dropping: HashMap::new(),
            drop_cooldown: HashMap::new(),
            shutting_down: false,
            richest: HashMap::new(),
            longest_spin: HashMap::new(),
            slots_cooldown: HashMap::new(),
        }
    }
}

pub struct BotInfoContainer;

impl TypeMapKey for BotInfoContainer {
    type Value = Arc<Mutex<BotInfo>>;
}

pub struct BotInfo {
    pub command_list: HashMap<String, Vec<String>>,
}

impl BotInfo {
    pub fn new(command_list: HashMap<String, Vec<String>>) -> Self {
        BotInfo {
            command_list,
        }
    }
}