use serenity::framework::standard::{ CommandResult, macros::command };

use serenity::prelude::*;
use serenity::model::prelude::*;

// todo: add a bucket on this
#[command]
#[description("Forces a refresh of roles used by the bot.")]
#[only_in(guilds)]
#[usage("refreshroles")]
async fn refreshroles(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let db = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<crate::BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let guild_id = msg.guild_id.unwrap();

    info!("Used refreshroles command in guild {}.", guild_id.0);

    match crate::system::roles::cache_roles_for(ctx.http.clone(), db.clone(), bot_cache.clone(), guild_id).await {
        Ok(_) => {
            msg.react(&ctx.http, ReactionType::Unicode("👍".to_owned())).await?;
        }

        Err(e) => {
            error!("Error refreshing roles -- {}", e);
            msg.react(&ctx.http, ReactionType::Unicode("⚠️".to_owned())).await?;
        }
    }

    Ok(())
}