mod ping; use ping::*;
mod deathmatch; use deathmatch::*;
mod spin; use spin::*;
mod topspin; use topspin::*;
mod top; use top::*;
mod cooldown; use cooldown::*;
mod info; use info::*;

use serenity::framework::standard::macros::group;

#[group]    
#[commands(ping, deathmatch, spin, topspin, top, cooldown, info)]
struct General;

const SPINNER_COOLDOWN: i64 = 5;
