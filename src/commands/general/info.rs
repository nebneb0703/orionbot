use serenity::{

    framework::standard::{
        CommandResult, macros::command
    },

    client::bridge::gateway::ShardId,
};

use serenity::prelude::*;
use serenity::model::prelude::*;

const VERSION: &'static str = std::env!("CARGO_PKG_VERSION");

#[command]
#[description("Displays bot info")]
#[usage("info")]
async fn info(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let shard_manager = data.get::<crate::ShardManagerContainer>().expect("Couldn't retrieve ShardManagerContainer");

    let lock = shard_manager.lock().await;
    let shard_runners = lock.runners.lock().await;

    let runner = shard_runners.get(&ShardId(ctx.shard_id)).unwrap();

    let latency = match runner.latency { Some(l) => format!("{}ms", l.as_millis()) , None => "N/A".to_owned() };

    let bot = ctx.http.get_current_user().await?;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Orion Bot!")
        .field("Creator", "~Nebula~#0703", false)
        .field("Contributors", "0xffset#2267", false)
        .field("Shard", format!("`{}/{}` Latency `{}`", ctx.shard_id, shard_runners.len(), latency), false)
        .field("Version", format!("`{}`", VERSION), false)
        .thumbnail(bot.avatar_url().unwrap_or(bot.default_avatar_url()))
        .colour(crate::EMBED_COLOUR)
    )).await?;
    Ok(())
}