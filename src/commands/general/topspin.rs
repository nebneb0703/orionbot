use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use chrono::Duration;

use nebbot_utils::time::human_readable;

#[command]
#[only_in(guilds)]
#[description("View spinner leaderboard!")]
#[usage("topspin")]
async fn topspin(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let spinners = database.select_top_10_spinners(msg.guild_id.unwrap().0).await?;

    let mut fields = Vec::new();

    for (i, spinner) in spinners.iter().enumerate() {
        let id = UserId(spinner.discord_id.0);

        let tag = match id.to_user(&ctx).await {
            Ok(u) => u.tag(),
            Err(_e) => {
                //warn!("Could not get user {} from REST API or guild cache: {}", spinner.discord_id.0, e);
                continue;
            }
        };

        fields.push((format!("{}. {}", i+1, tag), format!("Duration: {}\nChannel: <#{}>\nKnocked by: <@{}>\n",
            human_readable(Duration::seconds(spinner.duration)),
            spinner.channel_id.0, spinner.knocked_user_id.0)));
    }

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
        e
        .title("Spinner Leaderboard")
        .description("Top 10 spinners")
        .colour(crate::EMBED_COLOUR);

        for field in fields {
            e.field(field.0, field.1, false);
        }

        e
    })).await?;

    Ok(())
}