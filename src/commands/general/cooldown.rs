use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use chrono::prelude::*;
use chrono::Duration;

use nebbot_utils::time::human_readable;

#[command]
#[only_in(guilds)]
#[aliases("timeouts","timeout","cooldowns","timer","timers")]
#[description("Shows you all of your cooldowns for certain commands!")]
#[usage("cooldown")]
async fn cooldown(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = msg.author.id.0;
    let guild_id = msg.guild_id.unwrap().0;

    let user = database.get_user_or_default(discord_id, guild_id).await?;

    let now = Utc::now();

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
        e
        .title("Cooldown")
        .description("Commands with cooldown: ")
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        );

        e.field("Hourly", if now - user.last_hourly >= Duration::hours(1) { "Available".to_owned() } 
            else { human_readable(Duration::hours(1) - (now - user.last_hourly)) }, false);                    

        e.field("Mine", if now - user.last_mine >= Duration::hours(super::super::economy::MINE_COOLDOWN) { "Available".to_owned() } 
            else { human_readable(Duration::hours(super::super::MINE_COOLDOWN) - (now - user.last_mine)) }, false);

        e.field("Spin", if now - user.last_spin >= Duration::minutes(super::SPINNER_COOLDOWN) { "Available".to_owned() } 
            else { human_readable(Duration::minutes(super::SPINNER_COOLDOWN) - (now - user.last_spin)) }, false)
    })).await?;

    Ok(())
}
