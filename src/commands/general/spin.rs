use serenity::{

    framework::standard::{
        CommandResult, macros::command
    },

    collector::MessageCollectorBuilder,
    futures::stream::StreamExt,
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use chrono::prelude::*;
use chrono::Duration;

use crate::cache::*;
use crate::db::*;

use nebbot_utils::time::human_readable;
use nebbot_utils::types::TextU64;

use parking_lot::Mutex as BlockingMutex;

use diesel::SaveChangesDsl;

#[command]
#[aliases("spinner")]
#[description("Challenge yourself to maintain your spinner alive for as long as possible! Typing in the channel will knock over the spinner.")]
#[usage("spin")]
#[only_in(guilds)]
async fn spin(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let discord_id = msg.author.id.0;
    let guild_id = msg.guild_id.unwrap().0;
    
    if let Some(spinning) = bot_cache.lock().await.spinning.get(&guild_id) {
        if spinning.lock().contains(&discord_id) {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Spinner")
                .description("You are already spinning!")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;
    
            return Ok(());
        }
    }

    match database.get_user(discord_id, guild_id).await? {
        Some(user) => {
            let duration = Utc::now() - user.last_spin;

            if duration <= Duration::minutes(super::SPINNER_COOLDOWN) {
                let time_next = Duration::minutes(super::SPINNER_COOLDOWN) - duration;

                msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                    .title("Spin")
                    .description(format!("You need to wait {}",
                        human_readable(time_next)))
                    .colour(crate::EMBED_COLOUR)
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                )).await?;

                return Ok(());
            }

            let mut update = user.update();

            update.last_spin = Some(Utc::now());

            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;
        }

        None => {
            let mut default = DbUser::default(discord_id, guild_id);

            default.last_spin = Utc::now();

            database.add_user(default.clone()).await?;
        }
    }

    let start_time = Utc::now();

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Spinner")
        .description(format!("{}, your spinner is spinning! Do not allow for other users to message here, or they will knock it over!",
            msg.author.mention()))
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await?;

    {
        let mut lock = bot_cache.lock().await;
        
        match lock.spinning.get(&guild_id) {
            Some(spinning) => {
                spinning.lock().push(discord_id);
            }
            None => {
                let vec = vec![discord_id];

                lock.spinning.insert(guild_id, BlockingMutex::new(vec));
            }
        }
    }

    let mut collector = MessageCollectorBuilder::new(&ctx)
        .channel_id(msg.channel_id)
        .filter(|message| 
            !message.author.bot
        )
        .await;

    let message = match collector.next().await {
        Some(m) => m,
        None => {
            error!("Spin collector none.");

            let lock = bot_cache.lock().await;
            let mut vec = lock.spinning.get(&guild_id).expect("Could not find guild id in spinning").lock();
            
            let index = vec.iter().position(|&x| x == discord_id).expect("Could not find discord id in spinning");
            
            vec.remove(index);

            return Ok(());
        }
    };

    if message.author.id == msg.author.id {
        {
            let lock = bot_cache.lock().await;
            let mut vec = lock.spinning.get(&guild_id).expect("Could not find guild id in spinning").lock();
    
            let index = vec.iter().position(|&x| x == discord_id).expect("Could not find discord id in spinning");
            
            vec.remove(index);
        }
        
        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title("Spinner")
            .description(format!("{}, do not try to hide your message!",
                message.author.mention()))
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        return Ok(());
    }

    let duration = Utc::now() - start_time;

    let spin = match database.get_spinner(discord_id, guild_id).await? {
        Some(mut spinner) => {
            if duration.num_seconds() >= spinner.duration {
                spinner.channel_id = TextU64(msg.channel_id.0);
                spinner.knocked_user_id = TextU64(message.author.id.0);
                spinner.duration = duration.num_seconds();

                Some(spinner.save_changes(&*database.conn.lock().await)?)
            }
            else { None }
        }

        None => {
            let new = DbSpinner {
                discord_id: TextU64(discord_id),
                guild_id: TextU64(guild_id),
                channel_id: TextU64(msg.channel_id.0),
                knocked_user_id: TextU64(message.author.id.0),
                duration: duration.num_seconds()
            };
            
            database.add_spinner(new.clone()).await?;

            Some(new)
        }
    };

    let mut update = DbUserUpdate::default(discord_id, guild_id);

    update.last_spin = Some(Utc::now());

    let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

    {
        let lock = bot_cache.lock().await;
        let mut vec = lock.spinning.get(&guild_id).expect("Could not find guild id in spinning").lock();
        
        let index = vec.iter().position(|&x| x == discord_id).expect("Could not find discord id in spinning");
        
        vec.remove(index);
    }

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Spinner")
        .description(format!("Uh oh! {} knocked over {}'s spinner! It lasted {}!",
            message.author.mention(), msg.author.mention(), human_readable(duration)))
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await?;

    if let Some(s) = spin {
        crate::system::roles::check_longest_spinner(&ctx, s).await
    }
    else { Ok(()) }
}