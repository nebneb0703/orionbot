use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use nebbot_utils::discord::{ match_discord_user, MatchDiscordUser };

#[command]
#[aliases("bal", "b", "nuggets", "n", "wallet")]
#[description("Check your balance")]
#[usage("balance")]
#[only_in(guilds)]
async fn balance(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let dc_user = match match_discord_user(&args, &ctx, 0).await {
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) | MatchDiscordUser::NoUserFound |
        MatchDiscordUser::InvalidForm(_) => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Balance")
                .description("Invalid User Argument")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        },
        MatchDiscordUser::SameAsHost | 
        MatchDiscordUser::NoArgument => msg.author.clone(),
    };

    let id = dc_user.id.0;
    let guild_id = msg.guild_id.unwrap().0;

    let user = database.get_user_or_default(id, guild_id).await?;

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Balance")
        .description(format!("{} has {} Nuggets! :gem:", dc_user.mention(), user.nuggets))
        .colour(crate::EMBED_COLOUR)
    )).await?;

    Ok(())
}