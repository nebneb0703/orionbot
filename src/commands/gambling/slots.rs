use serenity::framework::standard::{
    Args, CommandResult, 
    macros::{ command, hook }
};

use serenity::model::prelude::*;
use serenity::prelude::*;
use serenity::utils::Colour;

use std::fmt;

use rand::prelude::*;

use crate::db::DbUser;

use diesel::SaveChangesDsl;

use std::collections::HashMap;

use chrono::prelude::*;

const SLOTS_DELAY: u64 = 1250;

#[derive(Hash, Clone, Copy, Debug, PartialEq, Eq)]
enum Symbols {
    Tangerine,
    Lemon,
    Watermelon,
    Banana,
    Grapes,
    Cherries,
    Pineapple,
    Ring,
    Gem,
    BlackLargeSquare,
}

impl fmt::Display for Symbols {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Tangerine => ":tangerine:",
                Self::Lemon => ":lemon:",
                Self::Watermelon => ":watermelon:",
                Self::Banana => ":banana:",
                Self::Grapes => ":grapes:",
                Self::Cherries => ":cherries:",
                Self::Pineapple => ":pineapple:",
                Self::Ring => ":ring:",
                Self::Gem => ":gem:",
                Self::BlackLargeSquare => ":black_large_square:",
            }
        )
    }
}

impl Symbols {
    fn win(&self) -> i64 {
        match self {
            Self::Tangerine => 200,
            Self::Lemon => 200,
            Self::Watermelon => 250,
            Self::Banana => 200,
            Self::Grapes => 200,
            Self::Cherries => 500,
            Self::Pineapple => 200,
            Self::Ring => 1500,
            Self::Gem => 2000,
            Self::BlackLargeSquare => -1, // will never happen
        }
    }
}

const SYMBOLS: [Symbols; 9] = [
    Symbols::Tangerine,
    Symbols::Lemon,
    Symbols::Watermelon,
    Symbols::Banana,
    Symbols::Grapes,
    Symbols::Cherries,
    Symbols::Pineapple,
    Symbols::Ring,
    Symbols::Gem,
];

#[hook]
pub async fn slots_cooldown(ctx: &Context, msg: &Message) {
    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Slots")
        .description("Woah slow down there! This command is currently on a **server** cooldown, to prevent spam and rate limiting. Please try again soon.")
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await.expect("Failed to send slots cooldown message.");
}

#[command]                  
#[description(
"Try your luck with the slot machine! Match at least one in each column to win:

:gem: - 2000 Nuggets! :gem:
:ring: - 1500 Nuggets! :gem:
:cherries: - 500 Nuggets! :gem:
:watermelon: - 250 Nuggets! :gem:
Everything else - 200 Nuggets! :gem:

Cost - 75 Nuggets! :gem:"
)]
#[only_in(guilds)]
#[usage("slots")]
#[aliases("slot")]
async fn slots(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let bot_cache = data.get::<crate::BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let discord_id = msg.author.id.0;
    let guild_id = msg.guild_id.unwrap().0;

    let user = match database.get_user(discord_id, guild_id).await? {
        Some(u) => {
            u
        }
        None => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Slots")
                .description(format!("You don't have enough Nuggets! :gem: Come back when you have {} Nuggets! :gem:", super::SLOT_PRICE))
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        }
    };

    if super::SLOT_PRICE > user.nuggets {
        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title("Slots")
            .description(format!("You don't have enough Nuggets! :gem: Come back when you have {} Nuggets! :gem:", super::SLOT_PRICE))
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        return Ok(());
    }

    {
        let mut lock = bot_cache.lock().await;

        match lock.slots_cooldown.get(&guild_id) {
            Some(t) => {
                if Utc::now() < *t {
                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                        .title("Slots")
                        .description("Woah slow down there! This command is currently on a **server** cooldown, to prevent spam and rate limitting. Please try again soon.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    )).await?;

                    return Ok(());
                }
            },
            None => { }
        }

        lock.slots_cooldown.insert(guild_id, Utc::now() + chrono::Duration::seconds(20));
    }

    let mut update = user.update();

    update.nuggets = Some(user.nuggets - super::SLOT_PRICE);
    let _host: DbUser = update.save_changes(&*database.conn.lock().await)?;

    drop(user); 

    // Game code start
    let mut game_setup: Vec<Vec<Symbols>> = Vec::new();
    let mut final_slot_machine_state: Vec<Vec<Symbols>> = Vec::new();

    let mut random = rand::rngs::OsRng;

    // fill the game setup with random symbols 5x15 vectors of symbols
    for _ in 0..5 {
        let mut temp: Vec<Symbols> = Vec::new();
        for _ in 0..15 {
            temp.push(SYMBOLS[random.gen_range(0..SYMBOLS.len())]);
        }
        game_setup.push(temp);
    }

    // Send the inital slot machine
    let mut message = msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Slots")
        .description(format!("{}", progress_game(&game_setup, 0, &mut final_slot_machine_state)))
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await?;

    // iterate 15 times to progress the game in slot_embed(...)
    for i in 1..15 {
        tokio::time::sleep(tokio::time::Duration::from_millis(SLOTS_DELAY)).await;

        message.edit(&ctx.http, |m| m.embed(|e| e
            .title("Slots")
            .description(format!("{}", progress_game(&game_setup, i, &mut final_slot_machine_state)))
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;
    }

    let mut solution_map: HashMap<Symbols, Vec<bool>> = HashMap::new();
    let temp = vec![false; 5];

    // for each symbol in the slot machine
    for i in 0..5 {
        for j in 0..5 {
            // get or insert that symbol from/into the solution map
            let val = solution_map.entry(final_slot_machine_state[j][i]).or_insert(temp.clone());

            // set its slot to true aka, this symbol was found in this slot
            val[i] = true;
        }
    }

    // stores the winning symbols if any
    let mut winners: Vec<Symbols> = Vec::new();

    // for each entry in the solution dict, if the val is true aka if all slots have had at least 1 of the symbol, add the symbol to winners
    for (key, value) in &solution_map {
        if value.iter().all(|&i| i) {
            winners.push(*key);
        }
    }

    // replace all symbols that are not winning with a black square
    if winners.len() != 0 {
        for i in 0..5 {
            for j in 0..5 {
                if !winners.contains(&final_slot_machine_state[i][j]) {
                    final_slot_machine_state[i][j] = Symbols::BlackLargeSquare;
                }
            }
        }

        let mut win_amount = 0;
        for symbol in winners {
            win_amount += symbol.win();
        }

        let user = match database.get_user(discord_id, guild_id).await? {
            Some(u) => u,
            _ => return Ok(()),
        };

        update.nuggets = Some(user.nuggets + win_amount);
        let user: DbUser = update.save_changes(&*database.conn.lock().await)?;

        message.edit(&ctx.http, |m| m.embed(|e| e
            .title(format!("You won {} Nuggets! :gem:", win_amount))
            .description(format!("{}", get_slot_machine_representation(&final_slot_machine_state)))
            .colour(Colour::from_rgb(15, 180, 15))
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        crate::system::roles::check_richest(&ctx, user, false).await?;
    }
    else {
        message.edit(&ctx.http, |m| m.embed(|e| e
            .title("You Lost")
            .description(format!("{}", get_slot_machine_representation(&final_slot_machine_state)))
            .colour(Colour::from_rgb(180, 15, 15))
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        crate::system::roles::check_richest(&ctx, database.get_user(discord_id, guild_id).await?.unwrap(), true).await?;
    }

    Ok(())
}

fn progress_game(game: &Vec<Vec<Symbols>>, count: usize, final_out: &mut Vec<Vec<Symbols>>) -> String {
    let mut slots_state: Vec<Vec<Symbols>> = Vec::new();

    // some weird math i did a few month ago to "roll" the correct slot down, idk, dont change it its fine i guess
    for i in 0..5 {
        let mut temp: Vec<Symbols> = Vec::new();

        if count > 9 + i {
            for j in 0..5 {
                temp.push(game[i][9 + j].clone());
            }
        } else if count < i {
            for j in 0..5 {
                temp.push(game[i][j].clone());
            }
        } else {
            for j in 0..5 {
                temp.push(game[i][count - i + j].clone());
            }
        }

        slots_state.push(temp.clone());
    }

    *final_out = vec![vec![Symbols::BlackLargeSquare; 5]; 5];
    for i in 0..5 {
        for j in 0..5 {
            final_out[j][i] = slots_state[i][4 - j].clone();
        }
    }

    get_slot_machine_representation(&final_out)
}

fn get_slot_machine_representation(slots: &Vec<Vec<Symbols>>) -> String {
    slots.iter()
        .map(|row| row.iter()
            .map(|e| e.to_string())
            .collect::<Vec<String>>()
            .join("┃")
        )
        .collect::<Vec<String>>()
        .join("\n")
}
