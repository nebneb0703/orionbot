use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use rand::prelude::*;

use crate::db::DbUser;
use crate::cards::*;

use diesel::SaveChangesDsl;

#[command]
#[description("Play Blackjack! Bet some Nuggets! :gem: for a chance to double your balance!")]
#[aliases("bj")]
#[usage("blackjack <bet-amount>")]
#[only_in(guilds)]
async fn blackjack(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mut random = rand::rngs::OsRng;

    let discord_id = msg.author.id.0;
    let guild_id = msg.guild_id.unwrap().0;

    let host = {
        match database.get_user(discord_id, guild_id).await? {
            Some(u) => u,
            None => {
                msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                    .title("Blackjack")
                    .description("You don't have enough Nuggets! :gem:")
                    .colour(crate::EMBED_COLOUR)
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                )).await?;

                return Ok(())
            }
        }
    };
    
    let bet_amount = match args.current() {
        Some(b) => match b.parse::<i64>()  {
            Ok(i) => {
                if i < 2 {
                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                        .title("Blackjack")
                        .description("Amount out of range.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    )).await?;

                    return Ok(());
                }

                i
            },
            Err(_) => {
                msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                    .title("Blackjack")
                    .description("Invalid amount")
                    .colour(crate::EMBED_COLOUR)
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                )).await?;

                return Ok(());
            }
        },
        None => {
            let mut bet_message = msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Blackjack")
                .description("Enter the amount of Nuggets! :gem: you want to bet. (Min 2)")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(30)).await {
                Some(msg) => {
                    match msg.content.trim().split(" ").next() {
                        Some(c) => match c.parse::<i64>()  {
                            Ok(i) => {
                                if i < 2 {
                                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                                        .title("Blackjack")
                                        .description("Amount out of range.")
                                        .colour(crate::EMBED_COLOUR)
                                        .footer(|f| f
                                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                            .text(msg.author.tag())
                                        )
                                    )).await?;

                                    return Ok(());
                                }
            
                                i
                            },
                            Err(_) => {
                                msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                                    .title("Blackjack")
                                    .description("Invalid amount")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                )).await?;
            
                                return Ok(());
                            }
                        }
                        None => {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                                .title("Blackjack")
                                .description("Invalid amount")
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
        
                            return Ok(());
                        }
                    }
                }
                None => {
                    bet_message.edit(&ctx.http, |m| m.embed(|e| e
                        .title("Blackjack")
                        .description("~~Enter the amount of Nuggets! :gem: you want to bet. (Min 2)~~\n\nTime out.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    )).await?;
        
                    return Ok(());
                }
            }
        }
    };
    
    if bet_amount > host.nuggets {
        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title("Blackjack")
            .description("You don't have enough Nuggets! :gem:")
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await?;

        return Ok(());
    }

    let mut update = host.update();

    update.nuggets = Some(host.nuggets - bet_amount);
    let host: DbUser = update.save_changes(&*database.conn.lock().await)?;

    let mut deck = DECK.clone().to_vec();
    deck.append(&mut DECK.clone().to_vec());
    //deck.append(&mut DECK.clone().to_vec()); because @captain_cuckoo#4446 is annoying

    deck.shuffle(&mut random);

    let mut computer = vec![deck.pop().unwrap(), deck.pop().unwrap()];

    let mut player = vec![deck.pop().unwrap(), deck.pop().unwrap()];

    if hand_value(&player) == 21 {
        if hand_value(&computer) == 21 {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Blackjack")
                .description(format!("Dealer: {} {}\n\nYou: {} {}\n\nIts a tie!", 
                    computer[0], computer[1],
                    player[0], player[1]))
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            let mut update = host.update();

            update.nuggets = Some(host.nuggets + bet_amount);

            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

            // Net change in nuggets 0. Do not check for richest.

            return Ok(());
        }
        else {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Blackjack")
                .description(format!("Dealer: {} {}\n\nYou: {} {}\n\nBlackjack! You win!", 
                    computer[0], computer[1],
                    player[0], player[1]))
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            let mut update = host.update();

            update.nuggets = Some(host.nuggets + (bet_amount as f32 * 2.50).round() as i64);

            let user: DbUser = update.save_changes(&*database.conn.lock().await)?;

            return crate::system::roles::check_richest(&ctx, user, false).await;
        }
    };

    // Due to collectors waiting, the host object may now be out of date.
    // Therefore, we drop the object, and store the change in nuggets
    // and retrieve the user and update it again once the game has finished.
    drop(host);
    let mut money_delta = 0;

    let mut text = format!("Dealer: {} [##]\n\nYou: {} {}\n\n
            Hit (Take another card) ⬇️\n
            Stand (Do nothing) ❌", 
        computer[0],
        player[0], player[1]);

    let mut game = msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Blackjack")
        .description(text.clone())
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    ).reactions(vec![ReactionType::Unicode("⬇️".to_string()), ReactionType::Unicode("❌".to_string())])).await?;
    // ⬇️ ❌💲

    let mut finished = false;

    let mut player_value = hand_value(&player);
    let mut player_hand = String::new();

    for card in player.clone() {
        player_hand.push_str(&format!("{} ", card));
    }

    while !finished {
        match game.await_reaction(&ctx).timeout(std::time::Duration::from_secs(30)).author_id(msg.author.id).filter(
            |reaction| match &*reaction.emoji.as_data() {
                "⬇️" | "❌" => true,
                _ => false,
            }
        ).await 
        {
            Some(action) => {
                let reaction = &action.as_inner_ref();

                match &*reaction.emoji.as_data() {
                    "⬇️" => {
                        let new = deck.pop().unwrap();

                        player_hand.push_str(&format!("{} ", new));

                        player.push(new);

                        player_value = hand_value(&player);
                
                        text = format!("Dealer: {} [##]\n\nYou: {}\n\n
                                Hit (Take another card) ⬇️\n
                                Stand (Do nothing) ❌", 
                            computer[0],
                            player_hand.clone());

                        reaction.delete(&ctx.http).await?;
                    }
                    "❌" => {
                        finished = true;

                        reaction.delete(&ctx.http).await?;
                    }

                    _ => {}
                }
            }

            None => {
                finished = true;
            }
        }

        if player_value > 21 {
            game.edit(&ctx.http, |m| m.embed(|e| e
                .title("Blackjack")
                .description(format!("Dealer: {} {}\n\nYou: {}\n\nBusted!", 
                    computer[0], computer[1],
                    player_hand))
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return crate::system::roles::check_richest(&ctx, database.get_user(discord_id, guild_id).await?.unwrap(), true).await;
        }

        if player_value == 21 {
            break;
        }

        if !finished {
            game.edit(&ctx.http, |m| m.embed(|e| e
                .title("Blackjack")
                .description(text.clone())
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;
        }
    }

    let mut computer_value = hand_value(&computer);

    while computer_value < 17 {
        let next = deck.pop().unwrap();

        computer.push(next);
        
        computer_value = hand_value(&computer);
    }

    let mut computer_hand = String::new();
    
    for card in computer.clone() {
        computer_hand.push_str(&format!("{} ",card));
    }

    if computer_value > 21 {
        let message = format!("Dealer: {}\n\nYou: {}\n\nYou win!", 
            computer_hand, player_hand);
        
        if let Err(e) = game.edit(&ctx.http, |m| m.embed(|e| e
            .title("Blackjack")
            .description(message)
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await
        {
            error!("Blackjack Computer Bust error: {:?}", e);
        };

        money_delta += (bet_amount as f32 * 2.0).round() as i64;
    }

    else if player_value == computer_value {
        let message = format!("Dealer: {}\n\nYou: {}\n\nIts a tie!", 
            computer_hand, player_hand);
        
        if let Err(e) = game.edit(&ctx.http, |m| m.embed(|e| e
            .title("Blackjack")
            .description(message)
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await {
            error!("Blackjack Tie error: {:?}", e);
        }

        money_delta += bet_amount;
    }

    else if computer_value > player_value {
        let message = format!("Dealer: {}\n\nYou: {}\n\nYou lose!", 
            computer_hand, player_hand);
        
        if let Err(e) = game.edit(&ctx.http, |m| m.embed(|e| e
            .title("Blackjack")
            .description(message)
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await {
            error!("Blackjack Computer Wins error: {:?}", e);
        };

        return crate::system::roles::check_richest(&ctx, database.get_user(discord_id, guild_id).await?.unwrap(), true).await;
    }
    else {
        let message = format!("Dealer: {}\n\nYou: {}\n\nYou win!", 
            computer_hand, player_hand);
        
        if let Err(e) = game.edit(&ctx.http, |m| m.embed(|e| e
            .title("Blackjack")
            .description(message)
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        )).await {
            error!("Blackjack Player Wins error: {:?}", e);
        };

        money_delta += (bet_amount as f32 * 2.0).round() as i64;
    }

    let user = match database.get_user(discord_id, guild_id).await? {
        Some(user) => {
            let mut update = user.update();
            update.nuggets = Some(user.nuggets + money_delta);

            update.save_changes(&*database.conn.lock().await)?
        },
        None => {
            error!("Blackjack second get user none.");
            return Ok(());
        }
    };

    crate::system::roles::check_richest(&ctx, user, false).await
}

fn hand_value(cards: &Vec<Card>) -> i8 {
    let mut total: i8 = 0;
    let mut ace_amount = 0;

    for card in cards {
        total += match card.value {
            CardValue::Two => 2,
            CardValue::Three => 3,
            CardValue::Four => 4,
            CardValue::Five => 5,
            CardValue::Six => 6,
            CardValue::Seven => 7,
            CardValue::Eight => 8,
            CardValue::Nine => 9,
            CardValue::Ten => 10,
            CardValue::Jack => 10,
            CardValue::Queen => 10,
            CardValue::King => 10,
            CardValue::Ace => {
                ace_amount += 1;
                0
            }
        }
    }

    if ace_amount > 0 {
        if total + 11 * ace_amount <= 21 {
            total += 11 * ace_amount
        }

        else {
            for i in (1..(ace_amount+1)).rev() {
                if total + 11 * i > 21 {
                    total += 1;
                    continue;
                }
                else {
                    total += 11;
                    break;
                }
            }
        }
    }

    total
}