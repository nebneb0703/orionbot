use serenity::{
    framework::standard::{
        CommandResult, Args,
        macros::command
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use futures::{ FutureExt, future::BoxFuture };

use crate::db::*;

use nebbot_utils::discord::parse_channels;

// TODO: Check for embed limits and create a page view or something
// Might be wise to make that an object for reuse

#[command]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
#[owner_privilege(true)]
#[aliases("cmds", "cmd")]
#[usage("commands [name]")]
#[description(
"Create custom white/blacklists for commands.

Without any arguments, this displays current command configurations for the server.
Pass in a command name as an argument, or `all` or `other`, to configure the white/blacklists for those commands.

`all` overrides any command configurations for that channel, and all commands in that channel follow this configuration instead.
`other` defines the command configuration for remaining commands which do not already have an exlpicit configuration for that channel."
)]
async fn commands(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    
    if let Some(s) = args.current() {
        let specific = s.to_lowercase();

        let bot_info = data.get::<crate::BotInfoContainer>().expect("Couldn't retrieve BotInfoContainer");

        let command_name = {
            let lock = bot_info.lock().await;

            match lock.command_list.get_key_value(&specific) {
                Some(c) => c.0.clone(),
                None => {
                    match lock.command_list.iter().find_map(|(key, val)| if val.contains(&specific.to_owned()) { Some(key) } else { None }) {
                        Some(c) => c.clone(),
                        None => {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                                .title("Commands Configuration")
                                .description("Invalid argument")
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;

                            return Ok(());
                        }
                    }
                }
            }
        };

        return specific_config(ctx, msg, &command_name).await;
    }
    
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let guild_id = msg.guild_id.unwrap().0;

    let config = database.get_whitelists(guild_id).await?;

    let mut description = String::new();

    if config.len() == 0 {
        description = "There are no command configurations on this server. See help for instructions.".to_owned();
    }

    for (k, v) in config {
        if let Whitelist::Disabled = v.list_type {
            description.push_str(&format!("`{}` List Disabled\n\n", k));
            continue;
        }

        let list = if v.list.is_empty() { "Empty".to_owned() } else {
            let mut channel_list = String::new();

            for id in v.list {
                channel_list.push_str(&format!("<#{}> ", id));
            }

            channel_list
        };

        description.push_str(&format!("`{}` {}: {}\n\n", k, v.list_type, list));
    }

    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
        .title("Commands Configuration")
        .description(description)
        .colour(crate::EMBED_COLOUR)
        .footer(|f| f
            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
            .text(msg.author.tag())
        )
    )).await?;

    Ok(())
}

fn specific_config<'a> (ctx: &'a Context, msg: &'a Message, command_name: &'a str) -> BoxFuture<'a, CommandResult> {
    async move {
        if crate::commands::IGNORE_WHITELIST.iter().any(|x| x == &command_name) {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title("Configuration")
                .description("You cannot modify this command!")
                .colour(crate::EMBED_COLOUR)
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            )).await?;

            return Ok(());
        }

        let data = ctx.data.read().await;
        let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

        let guild_id = msg.guild_id.unwrap().0;

        let server_config = database.get_whitelists(guild_id).await?;

        let mut is_first = false;

        let config = match server_config.get(command_name) {
            Some(c) => c.clone(),
            None => {
                is_first = true;

                Configuration {
                    list_type: Whitelist::Disabled,
                    list: Vec::new()
                }
            }
        };

        let title = format!("`{}` Configuration", command_name);

        let mut description = String::new();

        match config.list_type {
            Whitelist::Whitelist => {
                description.push_str("Whitelist: ");

                if config.list.is_empty() {
                    description = "Empty".to_owned() ;
                }
                else {
                    for l in config.list.clone() {
                        description.push_str(&format!("<#{}> ", l));
                    }
                }
            },
            Whitelist::Blacklist => {
                description.push_str("Blacklist: ");

                if config.list.is_empty() {
                    description = "Empty".to_owned();
                }
                else {
                    for l in config.list.clone() {
                        description.push_str(&format!("<#{}>", l));
                    }
                }
            },
            Whitelist::Disabled => {
                description.push_str("No Active Configuration")
            },
        }

        let mut message = msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
            .title(title.clone())
            .description(description.clone())
            .colour(crate::EMBED_COLOUR)
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        ).components(|c| c
            .create_action_row(|ar| 
                match config.list_type {
                    Whitelist::Whitelist => {
                        ar.create_button(|b| b
                            .label("Disable Whitelist")
                            .emoji(ReactionType::Unicode("❌".to_owned()))
                            .custom_id("disable_config")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Convert to Blacklist")
                            .emoji(ReactionType::Unicode("⚫".to_owned()))
                            .custom_id("convert_blacklist")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Add to Whitelist")
                            .emoji(ReactionType::Unicode("➕".to_owned()))
                            .custom_id("add_config")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Remove from Whitelist")
                            .emoji(ReactionType::Unicode("➖".to_owned()))
                            .custom_id("remove_config")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Clear Whitelist")
                            .emoji(ReactionType::Unicode("♻️".to_owned()))
                            .custom_id("clear_config")
                            .style(ButtonStyle::Secondary)
                        )
                    }

                    Whitelist::Blacklist => {
                        ar.create_button(|b| b
                            .label("Disable Blacklist")
                            .emoji(ReactionType::Unicode("❌".to_owned()))
                            .custom_id("disable_config")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Convert to Whitelist")
                            .emoji(ReactionType::Unicode("⚪".to_owned()))
                            .custom_id("convert_whitelist")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Add to Blacklist")
                            .emoji(ReactionType::Unicode("➕".to_owned()))
                            .custom_id("add_config")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Remove from Blacklist")
                            .emoji(ReactionType::Unicode("➖".to_owned()))
                            .custom_id("remove_config")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Clear Blacklist")
                            .emoji(ReactionType::Unicode("♻️".to_owned()))
                            .custom_id("clear_config")
                            .style(ButtonStyle::Secondary)
                        )
                    }

                    Whitelist::Disabled => {
                        ar.create_button(|b| b
                            .label("Enable Whitelist")
                            .emoji(ReactionType::Unicode("⚪".to_owned()))
                            .custom_id("enable_whitelist")
                            .style(ButtonStyle::Secondary)
                        )
                        .create_button(|b| b
                            .label("Enable Blacklist")
                            .emoji(ReactionType::Unicode("⚫".to_owned()))
                            .custom_id("enable_blacklist")
                            .style(ButtonStyle::Secondary)
                        )
                    }
                }
            )
        )).await?;

        match message.await_component_interaction(&ctx).timeout(std::time::Duration::from_secs(60)).author_id(msg.author.id).await {
            Some(interaction) => {
                match &interaction.data {
                    Some(InteractionData::MessageComponent(MessageComponent { custom_id, ..})) => {
                        match &**custom_id {
                            "disable_config" => {
                                if config.list.is_empty() {
                                    database.remove_config(guild_id, command_name).await?;
                                }
                                else {
                                    database.update_config(guild_id, command_name, Whitelist::Disabled).await?;
                                }

                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully disabled list!")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
                            }

                            "add_config" => {
                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title.clone())
                                    .description("Enter a list of channels to add to the list")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;

                                let mut to_add = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                                    Some(m) => parse_channels(m.content.clone()),
                                    None => return Ok(()),
                                };
            
                                to_add.retain(|x| !config.list.contains(x));
            
                                database.add_channel_config(guild_id, command_name, to_add).await?;    
            
                                interaction.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully updated list!")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                )).await?;
                            }

                            "remove_config" => {
                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title.clone())
                                    .description("Enter a list of channels to remove from the list")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
            
                                let mut to_remove = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                                    Some(m) => parse_channels(m.content.clone()),
                                    None => return Ok(()),
                                };
            
                                let mut remainder = Vec::new();
            
                                let mut i = 0;
            
                                while i < to_remove.len() {
                                    if !config.list.contains(&to_remove[i]) {
                                        remainder.push(to_remove[i]);
            
                                        to_remove.remove(i);
                                    }
                                    else {
                                        i += 1;
                                    }
                                }
            
                                let extra = if remainder.is_empty() { "".to_owned() } else {
                                    let mut remainder_string = String::new();
                                    
                                    for c in remainder {
                                        remainder_string.push_str(&format!("<#{}>", c));
                                    }
            
                                    format!("The channels {} were not in the list to begin with.", remainder_string)
                                };
            
                                database.remove_channel_config(guild_id, command_name, to_remove).await?;
            
                                interaction.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                    .title(title)
                                    .description(format!("Successfully updated list! {}", extra))
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                )).await?;
                            }

                            "clear_config" => {
                                database.clear_config(guild_id, command_name).await?;

                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully cleared list!")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
                            }
                            
                            "convert_blacklist" => {
                                database.update_config(guild_id, command_name, Whitelist::Blacklist).await?;
            
                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully converted list to blacklist! The command is now disabled in the previously configured channels.")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
                            }

                            "enable_blacklist" => {
                                if is_first {
                                    database.add_command_config(guild_id, command_name, Whitelist::Blacklist).await?;
                                }
                                else {
                                    database.update_config(guild_id, command_name, Whitelist::Blacklist).await?;
                                }

                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully enabled blacklist!")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
                            }
                            
                            "convert_whitelist" => {
                                database.update_config(guild_id, command_name, Whitelist::Whitelist).await?;
            
                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully converted list to whitelist! The command is now enabled in the previously configured channels.")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
                            }

                            "enable_whitelist" => {
                                if is_first {       
                                    database.add_command_config(guild_id, command_name, Whitelist::Whitelist).await?;
                                }
                                else {
                                    database.update_config(guild_id, command_name, Whitelist::Whitelist).await?;
                                }

                                interaction.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                                    .title(title)
                                    .description("Successfully enabled whitelist!")
                                    .colour(crate::EMBED_COLOUR)
                                    .footer(|f| f
                                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                        .text(msg.author.tag())
                                    )
                                ))).await?;
                            }

                            _ => {}
                        }                    
                    
                        message.edit(&ctx.http, |m| m.components(|c| c.set_action_rows(vec![]))).await?;
                        specific_config(ctx, msg, command_name).await?;
                    },
                    _ => {}
                }
            }
            None => {
                message.edit(&ctx.http, |m| m.components(|c| c.set_action_rows(vec![]))).await?;
            },
        }

        Ok(())
    }.boxed()
}