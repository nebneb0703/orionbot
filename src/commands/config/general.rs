use serenity::{collector::MessageCollectorBuilder, framework::standard::{
    CommandResult, macros::command
}};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::{ parse_role, parse_channel };

use futures::StreamExt;

use nebbot_utils::prelude::*;

use crate::db::DbGuild;
use diesel::SaveChangesDsl;

#[command]
#[description("Displays general bot configuration options.")]
#[usage("")]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
#[owner_privilege(true)]
async fn server(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let guild_id = msg.guild_id.unwrap();
    
    let (name, icon) = match guild_id.to_guild_cached(&ctx).await {
        Some(g) => {
            let icon = g.icon_url();
            let name = g.name;

            (name, icon)
        }
        None => match guild_id.to_partial_guild(&ctx.http).await {
            Ok(g) => {
                let icon = g.icon_url();
                let name = g.name;

                (name, icon)
            }
            Err(_e) => {
                msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                    .title("Server Configuraion")
                    .description("Error retrieving server information.")
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                )).await?;

                return Ok(());
            }
        }
    };

    let guild_config = match database.get_guild(guild_id.0).await? {
        Some(g) => g,
        None => {
            let default = DbGuild::default(guild_id.0);

            database.add_guild(default.clone()).await?;

            default
        }
    };

    let mut paged_embed = PagedEmbed::new();

    paged_embed.add_page(|e| { 
        let mut description = format!("```\n{}```\n**__General__**\n\n", name);

        match &guild_config.custom_prefix {
            Some(prefix) => {
                description.push_str(&format!("Server Prefix: `{}`\n", prefix))
            }
            None => {
                description.push_str(&format!("Server Prefix: `{}` (default)\n", crate::DEFAULT_PREFIX))
            }
        }
        
        e
        .title("Server Configuraion")
        .description(description)
        .colour(crate::EMBED_COLOUR);

        if let Some(icon) = &icon{
            e.thumbnail(icon.clone());
        }

        e
    }, |ar| Some(ar
        .create_button(|b| b
            .label("Change Prefix")
            .custom_id("set_server_prefix")
            .style(ButtonStyle::Secondary)
        ) // TODO: Add remove custom prefix
    ));

    paged_embed.add_page(|e| { 
        let mut description = format!("```\n{}```\n**__Roles__**\n\n", name);

        match &guild_config.richest_role {
            Some(role) => {
                description.push_str(&format!("Richest Member Role: <@&{}>\n", role.0))
            }
            None => {
                description.push_str("Richest Member Role: `None`\n")
            }
        }

        match &guild_config.longest_spin_role {
            Some(role) => {
                description.push_str(&format!("Longest Spinning Member Role: <@&{}>\n", role.0))
            }
            None => {
                description.push_str("Longest Spinning Member Role: `None`\n")
            }
        }

        e
        .title("Server Configuraion")
        .description(description)
        .colour(crate::EMBED_COLOUR);

        if let Some(icon) = &icon {
            e.thumbnail(icon.clone());
        }

        e
    }, |ar| { ar
        .create_button(|b| b
            .label("Modify Richest Member Role")
            .custom_id("set_richest_role")
            .style(ButtonStyle::Secondary)
        );

        if let Some(_) = &guild_config.richest_role {
            ar.create_button(|b| b
                .label("Remove Richest Member Role")
                .custom_id("remove_richest_role")
                .style(ButtonStyle::Secondary)
            );
        }

        ar.create_button(|b| b
            .label("Modify Longest Spinning Member Role")
            .custom_id("set_longest_spin_role")
            .style(ButtonStyle::Secondary)
        );

        if let Some(_) = &guild_config.longest_spin_role {
            ar.create_button(|b| b
                .label("Remove Longest Spinning Member Role")
                .custom_id("remove_longest_spin_role")
                .style(ButtonStyle::Secondary)
            );
        }

        Some(ar)
    });

    paged_embed.add_page(|e| { 
        let mut description = format!("```\n{}```\n**__Logging__**\n\n", name);

        match &guild_config.invite_log_channel {
            Some(channel) => {
                description.push_str(&format!("Invite Log Channel: <#{}>\n", channel.0))
            }
            None => {
                description.push_str("Invite Log Channel: `None`\n")
            }
        }

        e
        .title("Server Configuraion")
        .description(description)
        .colour(crate::EMBED_COLOUR);

        if let Some(icon) = &icon {
            e.thumbnail(icon.clone());
        }

        e
    }, |ar| { 
        ar.create_button(|b| b
            .label("Modify Invite Log Channel")
            .custom_id("set_invite_log_channel")
            .style(ButtonStyle::Secondary)
        );

        if let Some(_) = &guild_config.invite_log_channel {
            ar.create_button(|b| b
                .label("Remove Invite Log Channel")
                .custom_id("remove_invite_log_channel")
                .style(ButtonStyle::Secondary)
            );
        }

        Some(ar)
    });

    // In case it gets updated while waiting for responses.
    drop(guild_config);

    paged_embed.message(ctx, msg, |i| {
        Some(async move {
            // Should already exist at this point.
            let mut guild_config = database.get_guild(guild_id.0).await?.unwrap();

            match &i.clone().data {
            Some(InteractionData::MessageComponent(MessageComponent { custom_id, ..})) => match &**custom_id {
                "set_server_prefix" => {
                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Enter the new desired prefix.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;

                    let mut collector = MessageCollectorBuilder::new(&ctx)
                        .author_id(msg.author.id)
                        .channel_id(msg.channel_id)
                        .timeout(std::time::Duration::from_secs(30))
                        .collect_limit(1)
                        .await;

                    if let Some(response) = collector.next().await {
                        let prefix = response.content.trim();

                        guild_config.custom_prefix = Some(prefix.to_owned());

                        let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                            .expect("Failed to update guild config.");

                        i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                            .title("Server Configuration")
                            .description(format!("Successfully updated prefix to `{}`", prefix))
                            .colour(crate::EMBED_COLOUR)
                            .footer(|f| f
                                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                .text(msg.author.tag())
                            )
                        )).await?;
                    }
                }

                "set_richest_role" => {
                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Mention the role to be used as the Richest Member role:")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;

                    let role = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                        Some(r) => parse_role(r.content.clone()),
                        None => return Ok(()),
                    };

                    match role {
                        Some(role_id) => {
                            guild_config.richest_role = Some(TextU64(role_id));

                            let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                            .expect("Failed to update guild config.");

                            i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                .title("Server Configuration")
                                .description(format!("Successfully updated Richest Member role to <@&{}>.", role_id))
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
                        }
                        None => {
                            i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                .title("Server Configuration")
                                .description("Invalid role.")
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
                        }
                    }
                }

                "set_longest_spin_role" => {
                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Mention the role to be used as the Longest Spinning Member role:")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;

                    let role = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                        Some(r) => parse_role(r.content.clone()),
                        None => return Ok(()),
                    };

                    match role {
                        Some(role_id) => {
                            guild_config.longest_spin_role = Some(TextU64(role_id));

                            let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                            .expect("Failed to update guild config.");

                            i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                .title("Server Configuration")
                                .description(format!("Successfully updated Longest Spinning Member role to <@&{}>.", role_id))
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
                        }
                        None => {
                            i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                .title("Server Configuration")
                                .description("Invalid role.")
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
                        }
                    }
                }

                "set_invite_log_channel" => {
                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Enter the channel to log invites in:")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;

                    let channel = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                        Some(r) => parse_channel(r.content.clone()),
                        None => return Ok(()),
                    };

                    match channel {
                        Some(channel_id) => {
                            guild_config.invite_log_channel = Some(TextU64(channel_id));

                            let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                            .expect("Failed to update guild config.");

                            crate::system::invites::cache_invites(ctx.http.clone(), ctx.cache.clone(), database.clone()).await;

                            i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                .title("Server Configuration")
                                .description(format!("Successfully updated Invite Log Channel to <#{}>.", channel_id))
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
                        }
                        None => {
                            i.create_followup_message(&ctx.http, |m| m.create_embed(|e| e
                                .title("Server Configuration")
                                .description("Invalid channel.")
                                .colour(crate::EMBED_COLOUR)
                                .footer(|f| f
                                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                    .text(msg.author.tag())
                                )
                            )).await?;
                        }
                    }
                }
                
                "remove_richest_role" => {
                    guild_config.richest_role = None;

                    let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                    .expect("Failed to update guild config.");

                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Successfully removed Richest Member role.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;
                }

                "remove_longest_spin_role" => {
                    guild_config.longest_spin_role = None;

                    let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                    .expect("Failed to update guild config.");

                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Successfully removed Longest Spinning Member role.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;
                }

                "remove_invite_log_channel" => {
                    guild_config.invite_log_channel = None;

                    let _: DbGuild = guild_config.save_changes(&*database.conn.lock().await)
                    .expect("Failed to update guild config.");

                    // This will just clear the currently cached invites, 
                    // as later it does a db check and then stop.
                    crate::system::invites::cache_invites(ctx.http.clone(), ctx.cache.clone(), database.clone()).await;

                    i.create_interaction_response(&ctx.http, |m| m.interaction_response_data(|i| i.create_embed(|e| e
                        .title("Server Configuration")
                        .description("Successfully removed Invite Log channel.")
                        .colour(crate::EMBED_COLOUR)
                        .footer(|f| f
                            .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                            .text(msg.author.tag())
                        )
                    ))).await?;
                }
                
                _ => {}
            }
            _ => {}
    } Ok(()) }.boxed())}).await?;

    Ok(())
}