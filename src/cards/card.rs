#![allow(dead_code)]

#[derive(Clone, Copy)]
pub enum CardValue {
    Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten,
    Jack, Queen, King,
}

#[derive(Clone, Copy)]
pub enum Suit {
    Clubs, Hearts, Spades, Diamonds,
}

#[derive(Clone, Copy)]
pub struct Card {
    pub value: CardValue,
    pub suit: Suit,
}

impl std::fmt::Display for Card {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}{}]", self.value.short_name(), self.suit.emoji_name())
    }
}

impl std::fmt::Debug for Card {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}{}]", self.value.short_name(), self.suit.emoji_name())
    }
}

impl CardValue {
    pub fn short_name(&self) -> &str {
        match self {
            CardValue::Ace => "A",
            CardValue::Two => "2",
            CardValue::Three => "3",
            CardValue::Four => "4",
            CardValue::Five => "5",
            CardValue::Six => "6",
            CardValue::Seven => "7",
            CardValue::Eight => "8",
            CardValue::Nine => "9",
            CardValue::Ten => "10",
            CardValue::Jack => "J",
            CardValue::Queen => "Q",
            CardValue::King => "K",
        }
    }

    pub fn full_name(&self) -> &str {
        match self {
            CardValue::Ace => "Ace",
            CardValue::Two => "2",
            CardValue::Three => "3",
            CardValue::Four => "4",
            CardValue::Five => "5",
            CardValue::Six => "6",
            CardValue::Seven => "7",
            CardValue::Eight => "8",
            CardValue::Nine => "9",
            CardValue::Ten => "10",
            CardValue::Jack => "Jack",
            CardValue::Queen => "Queen",
            CardValue::King => "King",
        }
    }
}

impl Suit {
    pub fn short_name(&self) -> &str {
        match self {
            Suit::Clubs => "C",
            Suit::Hearts => "H",
            Suit::Spades => "S",
            Suit::Diamonds => "D",
        }
    }

    pub fn full_name(&self) -> &str {
        match self {
            Suit::Clubs => "Clubs",
            Suit::Hearts => "Hearts",
            Suit::Spades => "Spades",
            Suit::Diamonds => "Diamonds",
        }
    }

    pub fn emoji_name(&self) -> &str {
        match self {
            Suit::Clubs => "♣️",
            Suit::Hearts => "♥️",
            Suit::Spades => "♠️",
            Suit::Diamonds => "♦️",
        }
    }
}

pub static DECK: [Card; 52] = [
    Card{ value: CardValue::Ace, suit: Suit::Clubs },
    Card{ value: CardValue::Two, suit: Suit::Clubs },
    Card{ value: CardValue::Three, suit: Suit::Clubs },
    Card{ value: CardValue::Four, suit: Suit::Clubs },
    Card{ value: CardValue::Five, suit: Suit::Clubs },
    Card{ value: CardValue::Six, suit: Suit::Clubs },
    Card{ value: CardValue::Seven, suit: Suit::Clubs },
    Card{ value: CardValue::Eight, suit: Suit::Clubs },
    Card{ value: CardValue::Nine, suit: Suit::Clubs },
    Card{ value: CardValue::Ten, suit: Suit::Clubs },
    Card{ value: CardValue::Jack, suit: Suit::Clubs },
    Card{ value: CardValue::Queen, suit: Suit::Clubs },
    Card{ value: CardValue::King, suit: Suit::Clubs },
    Card{ value: CardValue::Ace, suit: Suit::Hearts },
    Card{ value: CardValue::Two, suit: Suit::Hearts },
    Card{ value: CardValue::Three, suit: Suit::Hearts },
    Card{ value: CardValue::Four, suit: Suit::Hearts },
    Card{ value: CardValue::Five, suit: Suit::Hearts },
    Card{ value: CardValue::Six, suit: Suit::Hearts },
    Card{ value: CardValue::Seven, suit: Suit::Hearts },
    Card{ value: CardValue::Eight, suit: Suit::Hearts },
    Card{ value: CardValue::Nine, suit: Suit::Hearts },
    Card{ value: CardValue::Ten, suit: Suit::Hearts },
    Card{ value: CardValue::Jack, suit: Suit::Hearts },
    Card{ value: CardValue::Queen, suit: Suit::Hearts },
    Card{ value: CardValue::King, suit: Suit::Hearts },
    Card{ value: CardValue::Ace, suit: Suit::Spades },
    Card{ value: CardValue::Two, suit: Suit::Spades },
    Card{ value: CardValue::Three, suit: Suit::Spades },
    Card{ value: CardValue::Four, suit: Suit::Spades },
    Card{ value: CardValue::Five, suit: Suit::Spades },
    Card{ value: CardValue::Six, suit: Suit::Spades },
    Card{ value: CardValue::Seven, suit: Suit::Spades },
    Card{ value: CardValue::Eight, suit: Suit::Spades },
    Card{ value: CardValue::Nine, suit: Suit::Spades },
    Card{ value: CardValue::Ten, suit: Suit::Spades },
    Card{ value: CardValue::Jack, suit: Suit::Spades },
    Card{ value: CardValue::Queen, suit: Suit::Spades },
    Card{ value: CardValue::King, suit: Suit::Spades },
    Card{ value: CardValue::Ace, suit: Suit::Diamonds },
    Card{ value: CardValue::Two, suit: Suit::Diamonds },
    Card{ value: CardValue::Three, suit: Suit::Diamonds },
    Card{ value: CardValue::Four, suit: Suit::Diamonds },
    Card{ value: CardValue::Five, suit: Suit::Diamonds },
    Card{ value: CardValue::Six, suit: Suit::Diamonds },
    Card{ value: CardValue::Seven, suit: Suit::Diamonds },
    Card{ value: CardValue::Eight, suit: Suit::Diamonds },
    Card{ value: CardValue::Nine, suit: Suit::Diamonds },
    Card{ value: CardValue::Ten, suit: Suit::Diamonds },
    Card{ value: CardValue::Jack, suit: Suit::Diamonds },
    Card{ value: CardValue::Queen, suit: Suit::Diamonds },
    Card{ value: CardValue::King, suit: Suit::Diamonds },
];