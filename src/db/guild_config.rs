use crate::db::schema::{ guild_channel_config, guild_command_config };
use diesel::prelude::*;

use crate::db::*;

use std::collections::HashMap;

use nebbot_utils::types::TextU64;

#[derive(Clone, Debug, Queryable, Insertable, Identifiable)]
#[primary_key(guild_id, channel_id, command_name)]
#[table_name = "guild_channel_config"]
pub struct DbGuildChannelConfig {
    pub guild_id: TextU64,
    pub channel_id: TextU64,
    pub command_name: String,
}

impl DbGuildChannelConfig {
    pub fn new(guild_id: u64, channel_id: u64, command_name: &str) -> Self {
        DbGuildChannelConfig {
            guild_id: TextU64(guild_id),
            channel_id: TextU64(channel_id),
            command_name: command_name.to_owned()
        }
    }
}

#[derive(Clone, Debug, Queryable, AsChangeset, Insertable, Identifiable)]
#[primary_key(guild_id, command_name)]
#[table_name = "guild_command_config"]
pub struct DbGuildCommandConfig {
    pub guild_id: TextU64,
    pub command_name: String,
    pub is_whitelist: Whitelist,
}

impl DbGuildCommandConfig {
    pub fn new(guild_id: u64, command_name: &str, is_whitelist: Whitelist) -> Self {
        DbGuildCommandConfig {
            guild_id: TextU64(guild_id),
            command_name: command_name.to_owned(),
            is_whitelist
        }
    }
}

#[derive(Clone, Debug)]
pub struct Configuration {
    pub list_type: Whitelist,
    pub list: Vec<u64>,
}

pub type GuildConfiguration = HashMap<String, Configuration>;

type ChannelConfig = Vec<DbGuildChannelConfig>;
type CommandConfig = Vec<DbGuildCommandConfig>;

impl Db {
    pub async fn get_whitelists(&self, guild_id: u64) -> QueryResult<GuildConfiguration> {
        let channel_config: ChannelConfig = guild_channel_config::table
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .load::<DbGuildChannelConfig>(&*self.conn.lock().await)?;

        let command_config: CommandConfig = guild_command_config::table
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .load::<DbGuildCommandConfig>(&*self.conn.lock().await)?;

        let mut config = HashMap::new();

        for c in command_config {
            let list_type = c.is_whitelist.clone();
            let list = channel_config.iter().filter(|x| x.command_name == c.command_name)
                .map(|x| x.channel_id.0).collect();

            config.insert(c.command_name.clone(), Configuration { list_type, list });
        }

        Ok(config)
    }

    pub async fn is_command_enabled(&self, guild_id: u64, channel_id: u64, command_name: &str) -> QueryResult<bool> {
        if crate::commands::IGNORE_WHITELIST.iter().any(|x| x == &command_name) {
            return Ok(true);
        }

        let channel_config: ChannelConfig = guild_channel_config::table
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_channel_config::command_name.eq_any(vec!["all", "other", &command_name]))
            .load::<DbGuildChannelConfig>(&*self.conn.lock().await)?;

        let command_config: CommandConfig = guild_command_config::table
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_command_config::command_name.eq_any(vec!["all", "other", &command_name]))
            .load::<DbGuildCommandConfig>(&*self.conn.lock().await)?;

        let mut config = HashMap::new();

        for c in command_config {
            let list_type = c.is_whitelist.clone();
            let list = channel_config.iter().filter(|x| x.command_name == c.command_name)
                .map(|x| x.channel_id.0).collect();

            config.insert(c.command_name.clone(), Configuration { list_type, list });
        }

        if let Some(all) = config.get("all") {
            match all.list_type {
                Whitelist::Whitelist => match all.list.iter().find(|&&x| x == channel_id) {
                    Some(_) => return Ok(true),
                    None => return Ok(false),
                },
                Whitelist::Blacklist => match all.list.iter().find(|&&x| x == channel_id) {
                    Some(_) => return Ok(false),
                    None => return Ok(true),
                },
                _ => {},
            }
        }

        if let Some(command) = config.get(&command_name.to_owned()) {
            match command.list_type {
                Whitelist::Whitelist => match command.list.iter().find(|&&x| x == channel_id) {
                    Some(_) => return Ok(true),
                    None => return Ok(false),
                },
                Whitelist::Blacklist => match command.list.iter().find(|&&x| x == channel_id) {
                    Some(_) => return Ok(false),
                    None => return Ok(true),
                },
                _ => {},
            }
        }

        if let Some(other) = config.get("other") {
            match other.list_type {
                Whitelist::Whitelist => match other.list.iter().find(|&&x| x == channel_id) {
                    Some(_) => return Ok(true),
                    None => return Ok(false),
                },
                Whitelist::Blacklist => match other.list.iter().find(|&&x| x == channel_id) {
                    Some(_) => return Ok(false),
                    None => return Ok(true),
                },
                _ => {},
            }
        }

        Ok(true)

    }

    pub async fn clear_config(&self, guild_id: u64, comamnd_name: &str) -> QueryResult<()> {
        diesel::delete(guild_channel_config::table)
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_channel_config::command_name.eq(comamnd_name))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn update_config(&self, guild_id: u64, command_name: &str, updated_is_whitelist: Whitelist) -> QueryResult<()> {
        diesel::update(guild_command_config::table)
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_command_config::command_name.eq(command_name))
            .set(guild_command_config::is_whitelist.eq(updated_is_whitelist))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn remove_config(&self, guild_id: u64, command_name: &str) -> QueryResult<()> {
        diesel::delete(guild_command_config::table)
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_command_config::command_name.eq(command_name))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn add_command_config(&self, guild_id: u64, command_name: &str, is_whitelist: Whitelist) -> QueryResult<()> {
        let data = DbGuildCommandConfig::new(guild_id, command_name, is_whitelist);

        diesel::insert_into(guild_command_config::table)
            .values(data)
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn add_channel_config(&self, guild_id: u64, command_name: &str, channels: Vec<u64>) -> QueryResult<()> {
        let mut data = Vec::new();

        for channel in channels {
            data.push(DbGuildChannelConfig::new(guild_id, channel, &command_name));
        }

        diesel::insert_into(guild_channel_config::table)
            .values(data)
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn remove_channel_config(&self, guild_id: u64, command_name: &str, channels: Vec<u64>) -> QueryResult<()> {
        diesel::delete(guild_channel_config::table)
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_channel_config::command_name.eq(command_name))
            .filter(guild_channel_config::channel_id.eq_any(channels.iter().map(|&x| TextU64(x))))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }
}