use chrono::{ DateTime, Utc, TimeZone };

use crate::db::schema::users;
use diesel::prelude::*;

use crate::db::*;

use nebbot_utils::types::TextU64;

#[derive(Clone, Debug, Queryable, Insertable, Identifiable)]
#[primary_key(discord_id, guild_id)]
#[table_name = "users"]
pub struct DbUser {
    pub discord_id: TextU64,
    pub nuggets: i64,
    pub last_mine: DateTime<Utc>,
    pub last_hourly: DateTime<Utc>,
    pub has_pickaxe: bool,
    pub last_spin: DateTime<Utc>,
    pub guild_id: TextU64,
    pub has_left: bool,
}

#[derive(AsChangeset, Clone, Identifiable)]
#[primary_key(discord_id, guild_id)]
#[table_name = "users"]
pub struct DbUserUpdate {
    pub discord_id: TextU64,
    pub nuggets: Option<i64>,
    pub last_mine: Option<DateTime<Utc>>,
    pub last_hourly: Option<DateTime<Utc>>,
    pub has_pickaxe: Option<bool>,
    pub last_spin: Option<DateTime<Utc>>,
    pub guild_id: TextU64,
    pub has_left: Option<bool>,
}

impl DbUserUpdate {
    pub fn default(discord_id: u64, guild_id: u64) -> Self {
        DbUserUpdate {
            discord_id: TextU64(discord_id),
            guild_id: TextU64(guild_id),
            nuggets: None,
            last_mine: None,
            last_hourly: None,
            has_pickaxe: None,
            last_spin: None,
            has_left: None,
        }
    }
}

impl DbUser {
    pub fn default(discord_id: u64, guild_id: u64) -> Self {
        DbUser {
            discord_id: TextU64(discord_id),
            nuggets: 0,
            last_mine: Utc.timestamp(0, 0),
            last_hourly: Utc.timestamp(0, 0),
            last_spin: Utc.timestamp(0, 0),
            has_pickaxe: false,
            guild_id: TextU64(guild_id),
            has_left: false,
        }
    }

    pub fn update(&self) -> DbUserUpdate {
        DbUserUpdate::default(self.discord_id.0, self.guild_id.0)
    }
}

impl Db {
    pub async fn get_user(&self, discord_id: u64, guild_id: u64) -> QueryResult<Option<DbUser>> {
        users::table.find((discord_id.to_string(), guild_id.to_string()))
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn get_user_or_default(&self, discord_id: u64, guild_id: u64) -> QueryResult<DbUser> {
        Ok(self.get_user(discord_id, guild_id).await?.unwrap_or(DbUser::default(discord_id, guild_id)))
    }

    pub async fn add_user(&self, user: DbUser) -> QueryResult<()> {
        diesel::insert_into(users::table).values(&user).execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn select_top_10_users(&self, guild_id: u64) -> QueryResult<Vec<DbUser>> {
        users::table.filter(users::guild_id.eq(TextU64(guild_id)))
            .filter(users::has_left.eq(false))
            .limit(10).order(users::nuggets.desc()).load(&*self.conn.lock().await)
    }
}