#![type_length_limit="1184549"] // For ubuntu server ¯\_(ツ)_/¯

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate log;

mod cache; use cache::*;
mod db; use db::Db;
mod system;
mod events;
mod cards;
mod commands;

use serenity::{
    client::bridge::gateway::GatewayIntents,
    framework::standard::StandardFramework,
    http::Http, utils::Colour,
};

use serenity::prelude::*;

use std::env;
use std::sync::Arc;
use std::collections::HashMap;
use std::collections::HashSet;

pub const DEFAULT_PREFIX: &'static str = "!";
pub const EMBED_COLOUR: Colour = Colour::from_rgb(52, 152, 219);

macro_rules! create_framework {
    ($framework:ident, $commands:ident, $owners:ident, $bot_id:ident, $($group:expr),*) => {
        let $framework = StandardFramework::new()
        .configure(|c| c
            .prefix("")
            .dynamic_prefix(|ctx, msg| Box::pin(async move {
                let data = ctx.data.read().await;
                let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

                let guild_id = match msg.guild_id {
                    Some(g) => g,
                    None => return Some(DEFAULT_PREFIX.to_owned()),
                };

                let guild = match database.get_guild(guild_id.0).await.expect("Failed to get guild for prefix from database.") {
                    Some(g) => g,
                    None => return Some(DEFAULT_PREFIX.to_owned()),
                };

                Some(guild.custom_prefix.unwrap_or(DEFAULT_PREFIX.to_owned()))
            }))
            .owners($owners)
            .on_mention(Some($bot_id))
            .case_insensitivity(true)
        )
        .help(&crate::commands::HELP)

        $(
            .group($group)
        )*

        // todo: maybe use a 5 second bucket just to prevent spam or something, might be wise to do that for all commands

        .after(commands::after)
        .before(commands::before)
        .normal_message(events::on_message);

        let mut $commands = HashMap::new();

        $commands.insert("other".to_owned(), vec![]);
        $commands.insert("all".to_owned(), vec![]);

        $(
            for c in $group.options.commands {
                $commands.insert(c.options.names[0].to_owned().clone(),
                    c.options.names[1..].iter().map(|&x| String::from(x)).collect());
            }
        )*
    };
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    // Create logs folder if it does not exist
    if !std::path::Path::new("logs/").exists() {
        std::fs::create_dir("logs").expect("Failed to create logs folder.");
    }

    let format = |out: fern::FormatCallback, message: &std::fmt::Arguments, record: &log::Record| {
        let level_colour = match record.level() {
            log::Level::Error => "\x1b[38;5;9m",
            log::Level::Warn => "\x1b[38;5;11m",
            log::Level::Debug => "\x1b[38;5;14m",
            
            log::Level::Info | 
            log::Level::Trace => "",
        };

        out.finish(format_args!(
            "\x1b[38;5;238m{}\x1b[0m [{}] {}{}\x1b[0m: {}",
            chrono::Utc::now().format("<%F %T>"),
            record.module_path().unwrap_or(record.target()),
            level_colour, record.level(),
            message
        ))
    };

    let now = chrono::Utc::now().timestamp();

    fern::Dispatch::new()
    .format(format)
    .level(log::LevelFilter::Warn)
    .level_for("orion_bot", log::LevelFilter::Debug)
    .chain(fern::log_file(format!("logs/debug_{}.txt", now)).expect("Failed to open log file."))
    .chain(
        fern::Dispatch::new()
        .level_for("orion_bot", log::LevelFilter::Info)
        .chain(std::io::stdout())
        .chain(fern::log_file(format!("logs/log_{}.txt", now)).expect("Failed to open log file."))
    )
    .apply()
    .expect("Failed to create fern dispatch.");

    let db = Db::new();

    let bot_cache = Mutex::new(BotCache::new());

    let token = env::var("DISCORD_TOKEN").expect("Token required in environmental variable DISCORD_TOKEN.");

    let http = Http::new_with_token(&token);

    let (owners, bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        },
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    create_framework![framework, commands, owners, bot_id,
        &crate::commands::GENERAL_GROUP,
        &crate::commands::ECONOMY_GROUP,
        &crate::commands::GAMBLING_GROUP,
        &crate::commands::CONFIGURATION_GROUP,
        &crate::commands::ADMIN_GROUP
    ];

    commands.insert("drop".to_owned(), vec![]);

    let mut client = Client::builder(&token)
        .event_handler(crate::events::Handler)
        .intents(GatewayIntents::all())
        .application_id(bot_id.0)
        .framework(framework).await
        .expect("Error creating client");

    info!("Created client!");

    let bot_info = Mutex::new(BotInfo::new(commands));

    info!("Listening for events!");

    let bot_cache_arc = Arc::new(bot_cache);

    {
        let mut data = client.data.write().await;

        data.insert::<DbContainer>(Arc::new(db));
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<BotCacheContainer>(bot_cache_arc.clone());
        data.insert::<BotInfoContainer>(Arc::new(bot_info));
    }

    if let Err(why) = client.start().await {
        error!("{:?}", why);
    }
}
