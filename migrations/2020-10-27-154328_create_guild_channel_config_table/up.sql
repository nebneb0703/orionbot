-- Your SQL goes here
CREATE TABLE guild_channel_config (
  guild_id text NOT NULL,
  channel_id text NOT NULL,
  command_name text NOT NULL,
  PRIMARY KEY(guild_id, channel_id, command_name)
);