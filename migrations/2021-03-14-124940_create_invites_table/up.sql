-- Your SQL goes here
CREATE TABLE invites (
  code text NOT NULL PRIMARY KEY,
  inviter text NOT NULL,
  uses bigint NOT NULL,
  guild_id text NOT NULL,
  max_age bigint NOT NULL,
  max_uses bigint NOT NULL,
  created_at timestamptz NOT NULL,
  temporary boolean NOT NULL
);